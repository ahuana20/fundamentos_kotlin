fun main() {
    var numero:Int = 120
    when (numero){
        in 1..50->println("Entre 0 y 50")
        in 50..100->println("Entre 50 y 100")
        in 100..150->println("Entre 100 y 150")
    }

    var letras:String = "b"
    when (letras){
        in "a".."d"->println("La letra es A,B,C,D")
        "b"->println("La letra es E")
        "c"->println("La letra es F")
        "d"->println("La letra es G")
    }
}