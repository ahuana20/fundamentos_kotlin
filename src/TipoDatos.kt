fun main() {
    // Tipos de datos
    var number:Int = 4
    var numberShort: Short = 32767
    var numberByte: Byte = 127
    var numberLong: Long = 9100000000000000000
    var decimalNumber : Double = 3.5
    var name: String = "Johann"
    var isActive: Boolean = true

}