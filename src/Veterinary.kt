fun main() {
    var nombre_veterinaria:String="San Benito"
    var Alto_veterinaria:Double=45.5
    var Ancho_veterinaria:Double=80.8
    var Materiales:String="Jeringa"
    var Analgesico:String="Anestesia"
    var ropita:String="chalequitos"
    var alimento:String="croquetas"
    var correas:String="Cuero"
    var placas:String="metal"
    var juguetes:String="plastico"
    var casita:String="plastico"
    var Canastillas:String="Metal"
    var Bozal:String="Cuero"
    var Personal:String="Veterinario"

    fun imprimir(mensaje: String){
        println(mensaje)
    }
    //FUNCIONES CON PARAMETRPS

    var saludo =""
    fun saludoVeterinaria(nombre: String):String{
        return "Hola Sr. $nombre, bienvenido a la Veterinaria San Benito"
    }
    saludo = saludoVeterinaria("Bernardo")
    imprimir(saludo)

    var horarioAtencion=""
    fun atender(horario:String,nombre: String):String{
        return "Recordarte Sr $nombre , que el horario de atencion es el siguiente: $horario"
    }

    horarioAtencion = atender("9am-5pm","Bernardo")
    imprimir(horarioAtencion)
    //dinero:Double

    var precioBaniado=""
    fun mostrarServicios(baniar:String,secado:String,costo:Double):String{
        return "El servicio de bañado S/. $baniar + S/. $secado = $costo"
    }

    precioBaniado= mostrarServicios("Manual","secadora caliente",45.00)
    imprimir(precioBaniado)

    fun promocionInternado(costoInternadoDia:Double, cantidadInternado:Int): Double {
        val Internado:Double =35.00
        var internadoDiario = cantidadInternado * costoInternadoDia
        imprimir("Además Comunicarle que el costo de internado diario de 1 mascota es de $Internado " +
                "pero por una promoción de  $cantidadInternado mascotas")
        return internadoDiario
    }
    imprimir("le costaría el monto de ${promocionInternado(costoInternadoDia = 25.00,2)} ")

    var a:Double
    fun alimentar(costoAlimento:Double, cantidadKg:Double):Double{
        var costoAlimentacion = costoAlimento * cantidadKg
        return costoAlimentacion
    }
    a=alimentar(8.0, 2.0)
    print("Adicional al costo de alimentacion diario (por mascota) que es de S/. $a ")

    fun despedidaVeterinaria(nombre: String):String{
        return "Muchas Gracias por su visita Sr. $nombre, esperamos verlo pronto"
    }

    var despedida = despedidaVeterinaria("Bernardo")
    imprimir(despedida)
}