fun main() {
    //forma implicita
    var numeros = 10
    var nombre = "Johan"
    var boolean = false
    var numeroDecimal = 4.3

    //forma explicita
    var numero2:Int = 10
    var nombre2:String = "Johan"
    var boolean2:Boolean = false
    var numeroDecimal2:Float = 4.3f

    println(numeros)
}